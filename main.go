package main

import (
	"net/http"
	"xmvc"
)

func main() {
	r := xmvc.Default()

	r.GET("/", func(c *xmvc.Context) {
		c.String(http.StatusOK, "Hello xMVC\n")
	})

	r.GET("/panic", func(c *xmvc.Context) {
		names := []string{"sfuxiao"}
		c.String(http.StatusOK, names[100])
	})

	_ = r.Run(":8080")
}
